var members = [{
  name: 'vasya',
  ban: ['kolya']
}, {
  name: 'kolya'
}, {
  name: 'tolka',
  ban: ['kolya', 'defka']
}, {
  name: 'defka'
}, {
  name: 'htos'
}];

function happyNewYear(members) {
  console.log('start');
  members.sort((a, b) => {
    if (a.ban) {
      if (b.ban) {
        return b.ban.length - a.ban.length;
      }
      return -1;
    } else {
      if (b.ban) {
        return 11;
      }
    }
    return 0;
  });
  let len = members.length;
  let finalMembers = [];
  let failedSearch;
  members.forEach((mem, i) => {
    let whom;
    let accept = false;
    let cycle = 0;
    while(!accept) {
      let index = Math.floor(Math.random() * len);
      whom = members[index].name;
      if (index !== i) {
        if (!(mem.ban && ~mem.ban.indexOf(whom))) {
          if (!finalMembers.find(m => m.whom === whom)) {
            let whomFromFinal = finalMembers.find(m => m.whom === mem.name);
            if (!(whomFromFinal && whom === whomFromFinal.name)) {
              accept = true;
              finalMembers.push({
                name: mem.name,
                whom: whom
              });
            }

          }
        } else {
          if (len - finalMembers.length === 1) {
            failedSearch = true;
            return;
          }
        }
      }
      if (cycle === 100) {
        console.log('fail');
        failedSearch = true;
        return;
      }
      cycle++;
    }
  });
  if (finalMembers.length === len) {
    let html = [];
    finalMembers.forEach(mem => {
      console.log(`${mem.name} => ${mem.whom}`);
      html.push(`<p>${mem.name} => ${mem.whom}</p>`)
    });
    document.body.innerHTML = html.join('');
  } else {
    return happyNewYear(members);
  }
}

happyNewYear(members);
